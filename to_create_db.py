from DB import DB_
from images import ImagesRepository
from texts import TextsRepository
from users import UsersRepository

content_dict = {'biology': 'биология', 'chemistry': 'химия', 'geography': 'география', 'history': 'история',
                'math': 'математика', 'philology': 'филология', 'physic': 'физика'}


db = DB_()
img_table = ImagesRepository(db.get_connection())
txt_table = TextsRepository(db.get_connection())
user_table = UsersRepository(db.get_connection())

#нужна была для первичного создания базы данных
def fill_DB():
    global txt_table, img_table, user_table
    img_table.init_table()
    txt_table.init_table()
    user_table.init_table()
    user_table.insert('Админ', '12.03.2019', 'static/admin.jpg')
    for name, ru_name in content_dict.items():
        text = ''.join(list(open('static/'+name+'.txt'))).split('\n---\n')
        for num in range(3):
            img_table.insert('static/'+name+str(num+1)+'.jpg', ru_name, 1)
            txt_table.insert(text[num], ru_name, 1)

