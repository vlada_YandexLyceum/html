from to_create_db import txt_table, img_table, user_table


def make_a_list(class_):
    def prepare(list1, list2):
        max_ = max(list(map(lambda x: len(x), [list2, list1])))
        print(max_)
        list1 += [''] * (max_ - len(list1))
        list2 += [''] * (max_ - len(list2))
        return list1, list2
    return_list = []
    text_list, image_list = prepare(txt_table.get_class(class_), img_table.get_class(class_))
    print(txt_table.get_class(class_), img_table.get_class(class_))
    for txt, img in (list(map(lambda x, y: (x, y), text_list, image_list))):
        list_of_tr_values = []
        for object in [txt, img]:
            if object != '':
                user = user_table.get(object[-1])
                user_name = user[1]
                user_avatar = user[3]
                list_of_tr_values.append((object[1], user_name, user_avatar))
            else:
                list_of_tr_values.append(('static/pekmen.png', 'Админ #данных здесь ещё нет, добавь!', 'static/admin.jpg'))
        return_list.append(list_of_tr_values)
    return return_list

def get_last_user_id():
    return user_table.get_all()[-1][0]

def get_last_image_id():
    return img_table.get_all()[-1][0]

def make_user(name, password, avatar):
    user_table.insert(name, password, avatar)

def is_user(name):
    if user_table.get_user(name) != None:
        return True
    else:
        return False

def chekc_about_users(name, password):
    if user_table.get_user(name) != None:
        inf_user = user_table.get_user(name)
        if password == inf_user[2]:
            return inf_user[1], inf_user[3]
    else:
        return False

def add_text(user_name, text, class_):
    print(user_table.get_user(user_name))
    user_id = user_table.get_user(user_name)[0]
    txt_table.insert(text, class_, user_id)

def add_image(user_name, image, class_):
    user_id = user_table.get_user(user_name)[0]
    print(12312413242341234)
    img_table.insert(image, class_, user_id)

#print(user_table.get_all())

