class ImagesRepository:
    # (id, image, class, user_id)
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS images4 
                            (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                             image VARCHAR(300),
                             class VARCHAR(300),
                             user_id VARCHAR(300)
                             )''')
        cursor.close()
        self.connection.commit()

    def insert(self, image, img_class, user_id):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO images4 
                          (image, class, user_id) 
                          VALUES (?,?,?)''', (image, img_class, user_id))
        cursor.close()
        self.connection.commit()

    def get_class(self, img_class):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM images4 WHERE class = ?", (img_class,))
        row = cursor.fetchall()
        return row

    def get_user(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM images4 WHERE user_id = ?", (user_id,))
        row = cursor.fetchall()
        return row

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM images4")
        rows = cursor.fetchall()
        return rows

    def delete_all(self):
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM images4''')
        cursor.close()
        self.connection.commit()