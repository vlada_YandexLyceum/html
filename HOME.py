from flask import Flask
from flask import request, redirect, url_for
from flask import render_template
from work_with_DB import make_a_list
from work_with_DB import get_last_user_id, add_image, add_text, get_last_image_id, make_user, is_user, chekc_about_users

app = Flask(__name__)
app.config['SECRET_KEY'] = 'yandexlyceum_secret_key'


def create_login_page(error=''):
    return render_template('login_page_.html', if_login=if_login_, is_login='True', people='Умники',
                           is_home='False',
                           error=error)


content_dict = {'biology': 'биология', 'chemistry': 'химия', 'geography': 'география', 'history': 'история',
                'math': 'математика', 'philology': 'филология', 'physic': 'физика'}

profession_dict = {'биология': 'Биологи', 'химия': 'Химики', 'география': 'Географы', 'история': 'Историки',
                   'математика': 'Математики', 'филология': 'Филологи', 'физика': 'Физики'}

if_login_ = ''


@app.route('/', methods=['POST', 'GET'])
def home_page():
    global request_text, if_login_
    if request.method == 'GET':
        return render_template('home_page.html', if_login=if_login_, choose_list=content_dict.values(), people='Умники',
                               is_home='True')
    elif request.method == 'POST':
        if len(request.form) == 1:
            request_text = request.form['text']
            url_ = '/шутники'
            return redirect(url_)
        else:
            return redirect('/login_page')


@app.route('/шутники', methods=['POST', 'GET'])
def other_page():
    global request_text, if_login_, USER_NAME
    print(request_text)
    people_ = profession_dict[request_text]
    if request.method == 'POST':

        # если текст
        if len(request.form) != 0:
            print(USER_NAME)
            add_text(USER_NAME, request.form['infa'], request_text)
            return render_template('other_page.html', if_login=if_login_, people=people_, is_home='False',
                                   content_list=make_a_list(request_text), is_login='True')

        # если картинка
        if len(request.files) == 1:
            print(USER_NAME)
            img_file = open('static/image' + str(get_last_image_id() + 1) + '.img', 'wb')
            print(USER_NAME, 0)
            img_file.write(request.files['file_fun'].read())
            print(USER_NAME, 1)
            add_image(USER_NAME, 'static/image' + str(get_last_image_id() + 1) + '.img', request_text)
            return render_template('other_page.html', if_login=if_login_, people=people_, is_home='False',
                                   content_list=make_a_list(request_text), is_login='True')
        return redirect('/')

    else:
        return render_template('other_page.html', if_login=if_login_, people=people_, is_home='False',
                               content_list=make_a_list(request_text), is_login='True')


@app.route('/login_page', methods=['POST', 'GET'])
def login_page():
    global if_login_, USER_NAME
    if request.method == 'POST':

        if len(request.form) == 0:  # на главную
            return redirect('/')

        if len(request.form) == 1:  # выход пользователя из аккаунта
            print(request.form)
            if 'exit' in request.form:
                print(0)
                if request.form['exit'] == '0':
                    USER_NAME = False
                    if_login_ = ''
                else:
                    create_login_page()

        if len(request.files) == 1 and request.form['login'] != '' and request.form['password'] != '':  # регистрация
            img_file = open('static/avatar' + str(get_last_user_id() + 1) + '.img', 'wb')
            img_file.write(request.files['file'].read())
            user_name = request.form['login']
            user_password = request.form['password']
            if is_user(user_name):
                return create_login_page('Пользователь с таким логином уже существует')
            else:
                make_user(user_name, user_password, 'static/avatar' + str(get_last_user_id() + 1) + '.img')
                return redirect('/')

        if len(request.files) == 0 and len(request.form) == 2:  # авторизация
            if request.form['password_true'] != '' and request.form['login_true'] != '':
                if_login_ = '/Выйти'
                inf_user = chekc_about_users(request.form['login_true'], request.form['password_true'])
                if inf_user:
                    USER_NAME = request.form['login_true']
                    print(USER_NAME)
                else:
                    return create_login_page('Такого пользователя не существует! Проверьте данные')
                return redirect('/')

        if len(request.files) == 0 or len(request.form) in [3, 1]:  # защита от неверного ввода
            return create_login_page('Были введены не все параметры')


    else:
        return create_login_page()


if __name__ == '__main__':
    app.run(port=8040, host='127.0.0.1')
