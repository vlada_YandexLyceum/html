class TextsRepository:
    # (id, image, class, user_id)
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS texts2 
                            (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                             text VARCHAR(300),
                             class VARCHAR(300),
                             user_id VARCHAR(300)
                             )''')
        cursor.close()
        self.connection.commit()

    def insert(self, text, txt_class, user_id):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO texts2 
                          (text, class, user_id) 
                          VALUES (?,?,?)''', (text, txt_class, user_id))
        cursor.close()
        self.connection.commit()

    def get_class(self, txt_class):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM texts2 WHERE class = ?", (txt_class,))
        row = cursor.fetchall()
        return row

    def get_user(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM texts2 WHERE user_id = ?", (user_id,))
        row = cursor.fetchall()
        return row

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM texts2")
        rows = cursor.fetchall()
        return rows

    def delete_all(self):
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM texts2''')
        cursor.close()
        self.connection.commit()