class UsersRepository:
    # (id, user, password, avatar)
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS users1 
                            (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                             user VARCHAR(300),
                             password VARCHAR(300), 
                             avatar VARCHAR(300)
                             )''')
        cursor.close()
        self.connection.commit()

    def insert(self, user, password, avatar):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO users1 
                          (user, password, avatar) 
                          VALUES (?,?,?)''', (user, password, avatar))
        cursor.close()
        self.connection.commit()

    def get(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users1 WHERE id = ?", (str(user_id),))
        row = cursor.fetchone()
        return row

    def get_user(self, user):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users1 WHERE user = ?", (user,))
        row = cursor.fetchone()
        return row

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users1")
        row = cursor.fetchall()
        return row

    def delete_all(self):
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM users1''')
        cursor.close()
        self.connection.commit()